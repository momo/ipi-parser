<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ipi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ipi_number')->unique();
            $table->string('name');
            $table->string('thoroughfare');
            $table->string('premise');
            $table->string('postal_code');
            $table->string('locality');
            $table->string('email');
            $table->boolean('syndic');
            $table->boolean('mediator');
            $table->string('phone');
            $table->string('mobile');
            $table->string('company');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ipi');
    }
}
