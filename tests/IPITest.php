<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IPITest extends TestCase
{
    /**
     * Test Case 0
     * Data Structure Test
     * Verify if data structure is correct
     * @return void
     */


    public function testDataStructure()
    {
        $this->get('/ipi/502763')
            ->seeJsonStructure([
               'id','ipi_number','name','thoroughfare','premise','postal_code','locality','email','syndic','mediator','phone','mobile','company'
            ]);
    }

    /**
     * Test Case 1
     * All Data Is available and correct
     * Test if the parsed data is correct
     */
    public function testAllData(){
        $this->get('/ipi/103002')
            ->seeJsonEquals([
                'id' => 6,
                'ipi_number' => '103002',
                'name' => 'Damien LEVIE',
                'thoroughfare' => 'Avenue Fr. Roosevelt',
                'premise' => '258',
                'postal_code' => '1050',
                'locality' => 'Bruxelles',
                'email' => 'damienlevie@yahoo.fr',
                'syndic' => 'Syndic (Titulaire)',
                'mediator' => 'Courtier (Titulaire)',
                'phone' => '+32 (2) 675.61.52',
                'mobile' => '+32 (475) 31.39.61',
                'company' => 'Levie Damien',
            ]);
    }

    /**
     * Test Case 2
     * Missing Courtier Information
     * IPI is Missing Courtier
     * mediator = courtier
     */
    public function testMissingMediator(){
        $this->get('/ipi/203835')
            ->seeJsonContains([
                'mediator'=>''
            ]);
    }

    /**
     * Test Case 3
     * Missing Syndic Information
     * IPI is Missing Syndic
     */
    public function testMissingSyndic(){
        $this->get('/ipi/505600')
            ->seeJsonContains([
                'syndic'=>''
            ]);
    }

    /**
     * Test Case 4
     * Missing Mobile Phone Number Test
     * IPI is Missing the Mobile Phone Number
     */
    public function testMissingMobile(){
        $this->get('/ipi/104195')
            ->seeJsonContains([
                'mobile'=>''
            ]);
    }
    /**
     * Test Case 5
     * Missing Phone Number Test
     * IPI is Missing the Phone Number
     */
    public function testMissingPhone(){
        $this->get('/ipi/101185')
            ->seeJsonContains([
                'phone'=>''
            ]);
    }
    /**
     * Test Case 6
     * Missing Courtier & Mobile Phone Number Test
     * IPI is Missing the Courtier & Mobile Phone Number
     */
    public function testMissingMediatorMobile(){
        $this->get('/ipi/501140')
            ->seeJsonContains([
                'mediator'=>'',
                'mobile'=>''
            ]);
    }
    /**
     * Test Case 7
     * Missing Courtier & Phone Number Test
     * IPI is Missing the Courtier & Phone Number
     */
    public function testMissingMediatorPhone(){
        $this->get('/ipi/507845')
            ->seeJsonContains([
                'mediator'=>'',
                'phone'=>''
            ]);
    }

    /**
     * Test Case 8
     * Missing Syndic & Mobile Phone Number Test
     * IPI is Missing the Syndic & Mobile Phone Number
     */
    public function testMissingSyndicMobile(){
        $this->get('/ipi/506978')
            ->seeJsonContains([
                'syndic'=>'',
                'mobile'=>''
            ]);
    }

    /**
     * Test Case 9
     * Missing Syndic & Mobile Phone Number Test
     * IPI is Missing the Syndic & Mobile Phone Number
     */
    public function testMissingSyndicPhone(){
        $this->get('/ipi/501992')
            ->seeJsonContains([
                'syndic'=>'',
                'phone'=>''
            ]);
    }

    /**
     * Test Case 10
     * Is Only a mediator without more info
     * IPI Mediator Only
     * We verify the other attributes are empty
     */
    public function testMediatorOnly(){
        $this->get('/ipi/510211')
            ->seeJsonContains([
                'syndic'=>'',
                'phone'=>'',
                'mobile'=>'',
                'email'=>''
            ]);
    }

    /**
     * Test Case 11
     * Is Only a mediator with mobile
     * IPI Mediator and Mobile Only
     * We verify the other attributes are empty
     */
    public function testMediatorMobile(){
        $this->get('/ipi/510211')
            ->seeJsonContains([
                'syndic'=>'',
                'phone'=>'',
                'email'=>''
            ]);
    }

    /**
     * Test Case 12
     * Is Only a mediator with a phone
     * IPI Mediator and Phone Only
     * We verify the other attributes are empty
     */
    public function testMediatorPhone(){
        $this->get('/ipi/100004')
            ->seeJsonContains([
                'syndic'=>'',
                'mobile'=>'',
                'email'=>''
            ]);
    }


    /**
     * Test Case 13
     * Missing mail & phone
     * IPI without mail and mobile
     */
    public function testMissingMailPhone(){
        $this->get('/ipi/502661')
            ->seeJsonContains([
                'phone'=>'',
                'email'=>''
            ]);
    }


    /**
     * Test Case 99
     * Wrong IPI Test
     * Return Empty Json and Error
     */
    public function testError(){
        $this->get('/ipi/100')
            ->seeJsonEquals([]);
    }






}
