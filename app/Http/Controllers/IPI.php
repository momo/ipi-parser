<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Goutte\Client;
use App\Http\Controllers\Controller;

class IPI extends Controller
{
    private $url_check='http://www.ipi.be/lagent-immobilier/recherchez-vous-un-agent-immobilier-agree-ipi-vous-les-trouverez-tous-ici?keys=';
    private $url_personal='http://www.ipi.be/agent-immobilier/';

    public function getIpiInfo($ipi)
    {
        $client  = new Client();
        $crawler = $client->request('GET',$this->url_check.$ipi);
        $sub = $crawler->filter('.results > .search-results-list > .item-list > ol');

        // Case Nothing Found
        if($sub->count()==0)
            return response()->json(null,404);

        $sub = $sub->children();

        // Case 1 Record is found
        if ($sub->count()==1)
        {

            $agentParse =   $sub->first();

            // Parsing Call
            return $this->addIPI($agentParse,$ipi);
        }

        // Case 2 or more records where found
        // Should add loop to iterate in system
        return response()->json(null,404);

    }

    /**
     * @param \Symfony\Component\DomCrawler\Crawler $agentParse
     * @param int $ipi
     * @return \App\IPIInfo
     */
    private function addIPI($agentParse, $ipi){
        // Retrive Previous IPI if it exists


        $ipiModel =  \App\IPIInfo::firstOrNew(['ipi_number'=>$ipi]);
        $test = $ipiModel->exists;

        // Retrive normal informations
        // we avoid accessing the parser as we require to verify the existence of each field
        // we use the internal function parseInfo to safely access the dom
        $ipiModel->ipi_number=$ipi;
        $ipiModel->name = $this->parseInfo($agentParse, '.name > a');
        $ipiModel->company = $this->parseInfo($agentParse, '.company-name > a');
        $ipiModel->thoroughfare = $this->parseInfo($agentParse, '.thoroughfare');
        $ipiModel->postal_code = $this->parseInfo($agentParse, '.postal-code');
        $ipiModel->premise = $this->parseInfo($agentParse, '.premise');
        $ipiModel->locality = $this->parseInfo($agentParse, '.locality');

        // Profession info
        // Recupere l'information si il est syndic ou courtier
        $ipiModel->syndic = $this->parseInfo($agentParse, '.syndic');
        $ipiModel->mediator = $this->parseInfo($agentParse, '.mediator');

        // second request
        // to retrive phone, email, mobile
        // we prepare the second request by replacing the space to dash - in the "name" field
        // then we invoke the second uri fname-lastname-IPI#Number
        $smallname = str_replace(' ','-',strtolower($ipiModel->name));
        $client  = new Client();
        $crawler = $client->request('GET',$this->url_personal.$smallname.'-'.$ipi);

        // collect second part of information
        $ipiModel->phone = $this->parseInfo($crawler, '.broker-address-teaser__phone > a');
        $ipiModel->mobile = $this->parseInfo($crawler, '.broker-address-teaser__mobile > a');
        $ipiModel->email = $this->parseInfo($crawler, '.broker-address-teaser__email > a');


        $ipiModel->save();
        return $ipiModel;
    }

    /**
     * Safe method to retrive content from parser, to avoid node empty error
     * Give string otherwise return empty
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     * @param string $rule
     * @return string
     */
    public function parseInfo($crawler, $rule){
        if ($crawler->filter($rule)->count()==1){
            return $crawler->filter($rule)->text();
        }
        return "";
    }
}
