<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IPIInfo extends Model
{
    protected $table = 'ipi';
    protected $fillable =  ['name','company','thoroughfare','postal_code','premise','locality','syndic','mediator','phone','mobile','email'];
    protected $hidden = ['created_at','updated_at'];
}
